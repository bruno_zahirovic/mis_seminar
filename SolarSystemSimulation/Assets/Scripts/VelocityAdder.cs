﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityAdder : MonoBehaviour
{
    [SerializeField] private float xVel;
    [SerializeField] private float yVel;
    [SerializeField] private float zVel;
    [SerializeField] private Rigidbody rb;



    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector3(xVel, yVel, zVel);
    }
}
