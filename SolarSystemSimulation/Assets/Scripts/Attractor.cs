﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    public Rigidbody rb;
    private float G = 0.05f;
    public static List<Attractor> Attractors;

    private void FixedUpdate()
    {
        foreach (Attractor attractor in Attractors)
        {
            if (attractor != this)
                Attract(attractor);
        }
    }

    private void OnEnable()
    {
        if(Attractors == null)
        {
            Attractors = new List<Attractor>();
        }
        Attractors.Add(this);
    }

    private void OnDisable()
    {
        Attractors.Remove(this);
    }
    void Attract(Attractor attractor)
    {
        Rigidbody bodyToAttract = attractor.rb;
        Vector3 direction = rb.position - bodyToAttract.position;
        float distanceSqrd = direction.sqrMagnitude;

        if (distanceSqrd == 0f) return;

        float forceMagnitude = G * (rb.mass * bodyToAttract.mass) / distanceSqrd;
        Vector3 force = direction.normalized * forceMagnitude;

        bodyToAttract.AddForce(force);
    }
}
