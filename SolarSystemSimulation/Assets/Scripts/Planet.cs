﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour

{
    public Rigidbody rb;
    [SerializeField] Vector3 velocity = new Vector3(0, 3.5f, 0);

    // Start is called before the first frame update
    void Start()
    {

        rb.velocity = velocity;
    }

}
