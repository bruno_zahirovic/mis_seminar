﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LR_PointPassing : MonoBehaviour
{
    [SerializeField] private Transform[] points;
    [SerializeField] private LineRendererController line;

    private void Start()
    {
        line.SetUpLine(points);
    }
}
